<?php

namespace Aldomar\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required',
            'short' => 'required',
            'body' => 'required'
        ];
    }

    public function messages() {
        return [
            'name.required' => 'Debe ingresar nombre del producto.',
            'short.required' => 'Debe ingresar un detalle del producto.',
            'body.required' => 'Debe ingresar una descripción del producto.'
        ];
    }

}
