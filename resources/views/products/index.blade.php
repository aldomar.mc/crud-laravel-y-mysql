@extends('layout')
@section('content')
<div class="col-lg-8">
    <h2>Listado de productos
        <a href="{{ route('products.create') }}" class="btn btn-info pull-right">Nuevo</a>
    </h2>
    @include('products.fragment.info')
    {!! $products->render() !!}
    <table class="table table-hover table-condensed">
        <thead>
            <tr>
                <th>ID</th>
                <th>Producto</th>
                <th colspan="3" class="text-center">Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td><strong>{{ $value->name }}</strong> {{ $value->short }}</td>
                <td>
                    <a href="{{ route('products.show', $value->id) }}" class="btn btn-success"><span class="glyphicon glyphicon-eye-open"></span></a>
                </td>
                <td>
                    <a href="{{ route('products.edit', $value->id) }}" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span></a>
                </td>
                <td>
                    @include('products.delete')
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="col-lg-4">
    @include('products.fragment.aside')
</div>
@endsection