<form action="{{ route('products.destroy', $value->id) }}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
    <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button>
</form>