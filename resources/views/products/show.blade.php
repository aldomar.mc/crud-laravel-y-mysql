@extends('layout')
@section('content')
<div class="col-lg-8">
    <h2>{{ $product->name }}
    <a href="{{ route('products.edit', $product->id) }}" class="btn btn-warning pull-right"><span class="glyphicon glyphicon-edit"></span></a>
    </h2>
    <p>{{ $product->short }}</p>
    {!! $product->body !!}
</div>
<div class="col-lg-4">
    @include('products.fragment.aside')
</div>
@endsection