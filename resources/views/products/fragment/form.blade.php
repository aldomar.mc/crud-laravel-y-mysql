<div class="form-group">
    {!! Form::label('name','Nombre del producto') !!}
    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Nombre del producto']) !!}
</div>
<div class="form-group">
    {!! Form::label('short','Detalle del producto') !!}
    {!! Form::text('short',null,['class'=>'form-control','placeholder'=>'Detalle del producto']) !!}
</div>
<div class="form-group">
    {!! Form::label('body','Descripción del producto') !!}
    {!! Form::textarea('body',null,['class'=>'form-control','placeholder'=>'Descripción del producto']) !!}
</div>
<div class="form-group">
    {!! Form::submit('Enviar',['class'=>'btn btn-primary']) !!}
</div>