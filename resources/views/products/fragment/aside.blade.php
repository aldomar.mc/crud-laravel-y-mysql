<div class="alert alert-info alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Aldomar ..!</strong> Desde aquí podemos listar, crear, actualizar y eliminar los productos.
</div>
<img src="{{ asset('aldomar.png') }}" alt="" class="img-responsive">

